package com.ahmedmousa.linkdevtask.articles.articlesList.domain.repo

import com.ahmedmousa.linkdevtask.articles.common.domain.entity.ArticleEntity
import io.reactivex.Single

/**
 * class is repo interface for getting list of articles
 * */
interface ArticlesListRepository{

    fun getArticlesList() : Single<List<ArticleEntity>>
}
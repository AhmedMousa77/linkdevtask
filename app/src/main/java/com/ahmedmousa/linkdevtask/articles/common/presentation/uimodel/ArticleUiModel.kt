package com.ahmedmousa.linkdevtask.articles.common.presentation.uimodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * class for model ArticleUiModel in presentation layer (it is Parcelable)
 * */
@Parcelize
data class ArticleUiModel (val author: String, val title: String,
                          val description: String, val url: String,
                          val urlToImage: String, val publishedAt: String):Parcelable
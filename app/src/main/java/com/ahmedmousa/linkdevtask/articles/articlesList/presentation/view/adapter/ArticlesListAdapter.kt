package com.ahmedmousa.linkdevtask.articles.articlesList.presentation.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import com.ahmedmousa.linkdevtask.R
import com.ahmedmousa.linkdevtask.articles.common.presentation.uimodel.ArticleUiModel
import com.ahmedmousa.linkdevtask.utils.AppUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import io.reactivex.subjects.PublishSubject

/**
 * class is adapter for articles list
 * */
class ArticlesListAdapter : RecyclerView.Adapter<ArticlesListAdapter.ArticlesListViewHolder>() , Filterable {

    private var listOfArticles : List<ArticleUiModel> = emptyList()
    private var listOfArticlesFiltered : List<ArticleUiModel> = emptyList()

    val clickEvent : PublishSubject<ArticleUiModel> = PublishSubject.create<ArticleUiModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticlesListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val myListItemView =  inflater.inflate(R.layout.view_article_item_layout , parent , false)

        return ArticlesListViewHolder(myListItemView)
    }

    override fun getItemCount(): Int {
        return listOfArticlesFiltered.size
    }

    override fun onBindViewHolder(viewHolder: ArticlesListViewHolder, position: Int) {
        val article : ArticleUiModel = listOfArticlesFiltered[position]
        viewHolder.bindView(article){
            clickEvent.onNext(article)
        }
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        clickEvent.onComplete()
    }

    fun setArticlesData(list : List<ArticleUiModel>){
        this.listOfArticles = list
        this.listOfArticlesFiltered = list
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): Filter.FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    listOfArticlesFiltered = listOfArticles
                } else {
                    val filteredList = ArrayList<ArticleUiModel>()
                    for (row in listOfArticles) {
                        if (row.title.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row)
                        }
                    }

                    listOfArticlesFiltered = filteredList
                }

                val filterResults = Filter.FilterResults()
                filterResults.values = listOfArticlesFiltered
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(charSequence: CharSequence, filterResults: Filter.FilterResults) {
                listOfArticlesFiltered = filterResults.values as List<ArticleUiModel>
                notifyDataSetChanged()
            }
        }
    }

    class ArticlesListViewHolder(view:View) : RecyclerView.ViewHolder(view){

        private val imvArticlePhoto : ImageView = view.findViewById(R.id.imvArticlePhoto)
        private val tvArticleTitle : TextView = view.findViewById(R.id.tvArticleTitle)
        private val tvArticleOwner : TextView = view.findViewById(R.id.tvArticleOwner)
        private val tvArticleDate : TextView = view.findViewById(R.id.tvArticleDate)

        fun bindView(article: ArticleUiModel, onItemClick:(View)->Unit) {

            tvArticleTitle.text = article.title
            val owner : String = "By : " +article.author
            tvArticleOwner.text = owner
            val date : String = AppUtils.formatDate(article.publishedAt ,
                    AppUtils.DATE_FORMAT_1 , AppUtils.DATE_FORMAT_2)
            tvArticleDate.text = date

            Glide.with(itemView).load(article.urlToImage).apply(
                    RequestOptions.placeholderOf(R.drawable.slicinglistingplaceholder)
            ).into(imvArticlePhoto)
            itemView.setOnClickListener(onItemClick)
        }

    }


}
package com.ahmedmousa.linkdevtask.articles.articlesList.presentation.view.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.ahmedmousa.linkdevtask.R
import com.ahmedmousa.linkdevtask.articles.articledetails.presentation.view.ArticleDetailsActivity
import com.ahmedmousa.linkdevtask.articles.articlesList.presentation.view.adapter.ArticlesListAdapter
import com.ahmedmousa.linkdevtask.articles.articlesList.presentation.viewmodel.ArticlesViewModel
import com.ahmedmousa.linkdevtask.base.data.model.BaseResponseHandler
import com.ahmedmousa.linkdevtask.articles.common.presentation.uimodel.ArticleUiModel
import com.ahmedmousa.linkdevtask.base.presentation.viewmodel.ViewModelFactory
import com.ahmedmousa.linkdevtask.utils.AppIntentConst.Companion.INTENT_ARTICLE_KEY
import com.ahmedmousa.linkdevtask.utils.AppNavigation
import dagger.android.AndroidInjection
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_article_list.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_article_list.*
import javax.inject.Inject
import android.app.SearchManager
import android.content.Context
import android.support.v7.widget.SearchView

/**
 * class for showing list of articles
 * */
class ArticlesActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory<ArticlesViewModel>

    private lateinit var articlesViewModel: ArticlesViewModel

    private lateinit var listItemClickEventDisposable : Disposable

    @Inject
    lateinit var articlesListAdapter : ArticlesListAdapter

    private lateinit var searchView: SearchView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidInjection.inject(this)

        setContentView(R.layout.activity_article_list)

        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        nav_view.setNavigationItemSelectedListener(this)


        //init rcv
        rcvArticles.layoutManager = LinearLayoutManager(this , LinearLayoutManager.VERTICAL , false)
        rcvArticles.adapter = articlesListAdapter

        //observe for onclick item
        listItemClickEventDisposable = articlesListAdapter.clickEvent.subscribe(this::onShowArticleDetails)

        //getting data
        articlesViewModel = ViewModelProviders.of(this , viewModelFactory).get(ArticlesViewModel::class.java)
        observeForArticlesFromServer()
    }

    private fun observeForArticlesFromServer(){

        articlesViewModel.getArticlesList().observe(this , Observer<BaseResponseHandler<List<ArticleUiModel>>> { responseHandler : BaseResponseHandler<List<ArticleUiModel>>? ->

            when(responseHandler){

                is BaseResponseHandler.Progress -> showLoadingBar(true)

                is BaseResponseHandler.Success ->{

                    showLoadingBar(false)

                    showView(true)
                    //set data
                    articlesListAdapter.setArticlesData(responseHandler.data)
                }

                is BaseResponseHandler.Failure ->{
                    articlesViewModel.resetLiveData()
                    showError(getString(R.string.connection_fail))
                }
            }

        })
    }

    private fun showLoadingBar(isShow : Boolean){

        if (isShow) progressBar.visibility = View.VISIBLE

        else progressBar.visibility = View.GONE
    }

    private fun showView(isShow : Boolean){

        if (isShow) rcvArticles.visibility = View.VISIBLE

        else rcvArticles.visibility = View.GONE
    }

    private fun showError(errorMsg : String){
        showLoadingBar(false)
        showView(false)
        tvError.visibility = View.VISIBLE
        imgBtnRefresh.visibility = View.VISIBLE
        tvError.text = errorMsg

        //refresh
        imgBtnRefresh.setOnClickListener {
            showLoadingBar(true)
            tvError.visibility = View.GONE
            imgBtnRefresh.visibility = View.GONE
            observeForArticlesFromServer()
        }
    }

    private fun onShowArticleDetails(articleUiModel: ArticleUiModel) {
        val bundle = Bundle()
        bundle.putParcelable(INTENT_ARTICLE_KEY , articleUiModel)
        AppNavigation.startActivity(this , ArticleDetailsActivity::class.java ,bundle)
    }

    private fun subscribeToAdapterClick(){
        if (listItemClickEventDisposable.isDisposed) listItemClickEventDisposable = articlesListAdapter.clickEvent.subscribe(this::onShowArticleDetails)
    }

    override fun onResume() {
        super.onResume()
        subscribeToAdapterClick()
    }

    override fun onStop() {
        super.onStop()
        with(listItemClickEventDisposable){
            takeIf { isDisposed }.apply { dispose() }
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.

        menuInflater.inflate(R.menu.main, menu)

        // Associate searchable configuration with the SearchView
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.maxWidth = Integer.MAX_VALUE

        // listening to search query text change
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{

            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                articlesListAdapter.filter.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                articlesListAdapter.filter.filter(query)
                return false
            }

        })

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_search -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.

        //show toast of selected item
        Toast.makeText(this , item.title , Toast.LENGTH_LONG).show()

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}

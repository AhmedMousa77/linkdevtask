package com.ahmedmousa.linkdevtask.articles.articlesList.data.repo

import com.ahmedmousa.linkdevtask.articles.articlesList.data.datasource.ArticlesListDataSource
import com.ahmedmousa.linkdevtask.articles.articlesList.domain.repo.ArticlesListRepository
import com.ahmedmousa.linkdevtask.articles.common.data.model.Article
import com.ahmedmousa.linkdevtask.articles.common.data.model.mapper.ArticleDataMapper
import com.ahmedmousa.linkdevtask.articles.common.domain.entity.ArticleEntity
import io.reactivex.Single

/**
 * class is repo implementation for getting list of articles and map it to domain  ArticleEntity model
 * */
class ArticlesListRepositoryImpl(private val usersListDataSource: ArticlesListDataSource, private val articleDataMapper: ArticleDataMapper) : ArticlesListRepository {

    override fun getArticlesList(): Single<List<ArticleEntity>> {
        return usersListDataSource.getArticlesList().map{ articles: List<Article> ->
            articleDataMapper.map(articles)
        }
    }

}
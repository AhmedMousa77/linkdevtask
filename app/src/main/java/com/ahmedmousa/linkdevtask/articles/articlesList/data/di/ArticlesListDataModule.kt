package com.ahmedmousa.linkdevtask.articles.articlesList.data.di

import com.ahmedmousa.linkdevtask.articles.articlesList.data.datasource.ArticlesListDataSource
import com.ahmedmousa.linkdevtask.articles.articlesList.data.datasource.remote.ArticlesListRemoteDataSource
import com.ahmedmousa.linkdevtask.articles.articlesList.data.datasource.remote.ArticlesListWebService
import com.ahmedmousa.linkdevtask.articles.articlesList.data.repo.ArticlesListRepositoryImpl
import com.ahmedmousa.linkdevtask.articles.articlesList.domain.repo.ArticlesListRepository
import com.ahmedmousa.linkdevtask.articles.common.data.model.mapper.ArticleDataMapper
import com.ahmedmousa.linkdevtask.articles.common.presentation.di.scope.ArticlesScope
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

/**
 * class for the data layer module to provide di for this layer
 * */
@Module
class ArticlesListDataModule {


    @Provides
    @ArticlesScope
    fun getWebService(retrofit: Retrofit) : ArticlesListWebService = retrofit.create(ArticlesListWebService::class.java)

    @Provides
    @ArticlesScope
    fun getArticlesListDataMapper() : ArticleDataMapper = ArticleDataMapper()

    @Provides
    @ArticlesScope
    fun getArticlesListRemoteDataSource(webservice: ArticlesListWebService): ArticlesListDataSource = ArticlesListRemoteDataSource(webservice)

    @Provides
    @ArticlesScope
    fun getArticlesListRepository(userDataSource: ArticlesListDataSource, articleDataMapper: ArticleDataMapper) : ArticlesListRepository =
            ArticlesListRepositoryImpl(userDataSource , articleDataMapper)

}
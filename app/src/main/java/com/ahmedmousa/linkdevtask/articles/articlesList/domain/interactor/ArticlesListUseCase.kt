package com.ahmedmousa.linkdevtask.articles.articlesList.domain.interactor

import com.ahmedmousa.linkdevtask.articles.articlesList.domain.repo.ArticlesListRepository
import com.ahmedmousa.linkdevtask.articles.common.domain.entity.ArticleEntity
import io.reactivex.Single
import javax.inject.Inject

/**
 * class is use case for getting list of articles
 * */
class ArticlesListUseCase @Inject constructor(private val articlesListRepository : ArticlesListRepository){

    fun build() : Single<List<ArticleEntity>>{
        return articlesListRepository.getArticlesList()
    }
}
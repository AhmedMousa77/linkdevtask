package com.ahmedmousa.linkdevtask.articles.common.presentation.di.scope

import javax.inject.Scope

/**
 * class for creating scope to Articles Module to use it in di
* */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ArticlesScope
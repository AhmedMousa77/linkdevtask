package com.ahmedmousa.linkdevtask.articles.articlesList.presentation.di


import com.ahmedmousa.linkdevtask.articles.articlesList.domain.interactor.ArticlesListUseCase
import com.ahmedmousa.linkdevtask.articles.articlesList.presentation.view.adapter.ArticlesListAdapter
import com.ahmedmousa.linkdevtask.articles.articlesList.presentation.viewmodel.ArticlesViewModel
import com.ahmedmousa.linkdevtask.base.data.model.BaseResponseHandler
import com.ahmedmousa.linkdevtask.base.data.networking.MyScheduler
import com.ahmedmousa.linkdevtask.articles.common.presentation.di.scope.ArticlesScope
import com.ahmedmousa.linkdevtask.articles.common.presentation.uimodel.ArticleUiModel
import com.ahmedmousa.linkdevtask.articles.common.presentation.uimodel.mapper.ArticleUiMapper
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

/**
 * class for the presentation layer module to provide di for this layer
 * */
@Module
class ArticlesListPresentationModule{

    @Provides
    @ArticlesScope
    fun provideArticlesListAdapter() : ArticlesListAdapter = ArticlesListAdapter()

    @Provides
    @ArticlesScope
    fun provideComposite() : CompositeDisposable = CompositeDisposable()

    @Provides
    @ArticlesScope
    fun getArticlesListUiMapper() : ArticleUiMapper = ArticleUiMapper()

    @Provides
    @ArticlesScope
    fun provideArticlesListPublishSubject() : PublishSubject<BaseResponseHandler<List<ArticleUiModel>>> = PublishSubject.create()

    @Provides
    @ArticlesScope
    fun provideArticlesListViewModel(articlesListUseCase: ArticlesListUseCase, scheduler: MyScheduler, compositeDisposable: CompositeDisposable,
                                     articlesListPublishSubject : PublishSubject<BaseResponseHandler<List<ArticleUiModel>>> ,
                                     articleUiMapper: ArticleUiMapper)
            : ArticlesViewModel = ArticlesViewModel(articlesListUseCase , compositeDisposable , articlesListPublishSubject , scheduler , articleUiMapper)

}
package com.ahmedmousa.linkdevtask.articles.common.presentation.uimodel.mapper

import com.ahmedmousa.linkdevtask.articles.common.domain.entity.ArticleEntity
import com.ahmedmousa.linkdevtask.articles.common.presentation.uimodel.ArticleUiModel
import javax.inject.Inject


/**
 * class for mapping model Article Entity in domain layer to model ArticleUiModel in presentation layer
 * */
class ArticleUiMapper @Inject constructor(){

    fun map(article : ArticleEntity) : ArticleUiModel {
        return ArticleUiModel(article.author, article.title, article.description,
                article.url, article.urlToImage, article.publishedAt)
    }

    fun map(articles : List<ArticleEntity>) : List<ArticleUiModel> {
        return articles.map { article: ArticleEntity -> map(article) }
    }
}
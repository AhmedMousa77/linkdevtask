package com.ahmedmousa.linkdevtask.articles.articlesList.data.datasource.remote

import com.ahmedmousa.linkdevtask.articles.articlesList.data.datasource.ArticlesListDataSource
import com.ahmedmousa.linkdevtask.articles.common.data.model.Article
import com.ahmedmousa.linkdevtask.articles.common.data.model.ArticlesResponse
import com.ahmedmousa.linkdevtask.base.data.networking.ApiInterface.Factory.API_KEY
import com.ahmedmousa.linkdevtask.base.data.networking.ApiInterface.Factory.SOURCE_API_1
import com.ahmedmousa.linkdevtask.base.data.networking.ApiInterface.Factory.SOURCE_API_2
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import javax.inject.Inject

/**
 * class is data source remote implementation for getting list of articles and zip two requests
 * */
class ArticlesListRemoteDataSource @Inject constructor(private val articlesListWebService: ArticlesListWebService) : ArticlesListDataSource {

    override fun getArticlesList(): Single<List<Article>> {

        val firstApi : Single<ArticlesResponse> = articlesListWebService.getArticles(SOURCE_API_1 , API_KEY)

        val secApi : Single<ArticlesResponse> = articlesListWebService.getArticles(SOURCE_API_2 , API_KEY)

        return Single.zip<ArticlesResponse, ArticlesResponse, List<Article>>(firstApi, secApi,
                BiFunction<ArticlesResponse, ArticlesResponse, List<Article>> { t1, t2 ->
                    val article : ArrayList<Article> = ArrayList()
                    article.addAll(t1.articles)
                    article.addAll(t2.articles)
                    return@BiFunction article.toList()
                })
    }

}
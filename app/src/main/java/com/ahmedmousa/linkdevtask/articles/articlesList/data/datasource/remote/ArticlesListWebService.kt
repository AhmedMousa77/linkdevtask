package com.ahmedmousa.linkdevtask.articles.articlesList.data.datasource.remote

import com.ahmedmousa.linkdevtask.articles.common.data.model.ArticlesResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * class is interface for requesting data using retrofit
 * */
interface ArticlesListWebService {

    @GET("articles")
    fun getArticles(@Query("source") source : String,
                    @Query("apiKey") apiKey : String): Single<ArticlesResponse>
}
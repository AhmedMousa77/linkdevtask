package com.ahmedmousa.linkdevtask.articles.articlesList.presentation.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.ahmedmousa.linkdevtask.articles.articlesList.domain.interactor.ArticlesListUseCase
import com.ahmedmousa.linkdevtask.base.data.extensions.*
import com.ahmedmousa.linkdevtask.base.data.model.BaseResponseHandler
import com.ahmedmousa.linkdevtask.base.data.networking.MyScheduler
import com.ahmedmousa.linkdevtask.articles.common.domain.entity.ArticleEntity
import com.ahmedmousa.linkdevtask.articles.common.presentation.uimodel.ArticleUiModel
import com.ahmedmousa.linkdevtask.articles.common.presentation.uimodel.mapper.ArticleUiMapper
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

/**
 * class is view model for articles list that handle requesting data and back data to ui
 * */
class ArticlesViewModel@Inject constructor(private val articlesListUseCase : ArticlesListUseCase,
                                           private val compositeDisposable: CompositeDisposable,
                                           private val articlesListPublishSubject : PublishSubject<BaseResponseHandler<List<ArticleUiModel>>>,
                                           private val myScheduler: MyScheduler ,
                                           private val articleUiMapper: ArticleUiMapper) : ViewModel(){

    private val articlesListLiveData : MutableLiveData<BaseResponseHandler<List<ArticleUiModel>>> by lazy {
        articlesListPublishSubject.toLiveData(compositeDisposable)
    }

    fun getArticlesList() : LiveData<BaseResponseHandler<List<ArticleUiModel>>> {

        if (articlesListLiveData.value == null){

            //requesting articles
            articlesListUseCase.build()
                    .performOnBackOutOnMain(myScheduler)
                    .subscribe ({ articles ->

                        //in case of success
                        articlesListPublishSubject.success(articles.map { article: ArticleEntity ->
                            articleUiMapper.map(article)
                        })

                    } , {error ->

                        //in case of failed
                        articlesListPublishSubject.failed(error)
                    }).addTo(compositeDisposable)
        }

        return articlesListLiveData
    }

    fun resetLiveData(){
        this.articlesListLiveData.value = null
    }

    override fun onCleared() {
        compositeDisposable.clear()
    }

}
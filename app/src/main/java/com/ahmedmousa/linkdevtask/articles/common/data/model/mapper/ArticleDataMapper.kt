package com.ahmedmousa.linkdevtask.articles.common.data.model.mapper


import com.ahmedmousa.linkdevtask.articles.common.data.model.Article
import com.ahmedmousa.linkdevtask.articles.common.domain.entity.ArticleEntity
import javax.inject.Inject

/**
 * class for mapping model Article in data layer to model Article Entity in domain layer
 * */
class ArticleDataMapper @Inject constructor(){

    fun map(article : Article) : ArticleEntity {
        return ArticleEntity(article.author, article.title, article.description,
                article.url, article.urlToImage, article.publishedAt)
    }

    fun map(articles : List<Article>) : List<ArticleEntity> {
        return articles.map { article: Article -> map(article) }
    }
}
package com.ahmedmousa.linkdevtask.articles.common.domain.entity

/**
 * class for model ArticleEntity in domain layer
 * */
data class ArticleEntity (val author: String, val title: String,
                          val description: String, val url: String,
                          val urlToImage: String, val publishedAt: String)

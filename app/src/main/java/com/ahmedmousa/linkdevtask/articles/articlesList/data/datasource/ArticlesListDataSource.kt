package com.ahmedmousa.linkdevtask.articles.articlesList.data.datasource

import com.ahmedmousa.linkdevtask.articles.common.data.model.Article
import io.reactivex.Single

/**
 * class is data source interface for getting list of articles
 * */
interface ArticlesListDataSource{

    fun getArticlesList() : Single<List<Article>>
}
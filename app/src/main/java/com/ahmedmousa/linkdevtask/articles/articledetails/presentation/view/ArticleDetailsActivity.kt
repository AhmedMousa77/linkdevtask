package com.ahmedmousa.linkdevtask.articles.articledetails.presentation.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.ahmedmousa.linkdevtask.R
import com.ahmedmousa.linkdevtask.articles.common.presentation.uimodel.ArticleUiModel
import com.ahmedmousa.linkdevtask.utils.AppIntentConst
import com.ahmedmousa.linkdevtask.utils.AppNavigation
import com.ahmedmousa.linkdevtask.utils.AppUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_article_details.*
import kotlinx.android.synthetic.main.content_article_details.*

/**
 * class for showing an article details
 * */
class ArticleDetailsActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article_details)

        //init  back btn
        initToolbar()

        //getting article info
        val bundle = intent.extras
        if (bundle != null)
            initView(bundle.getParcelable(AppIntentConst.INTENT_ARTICLE_KEY))
    }

    private fun initView(articleUiModel: ArticleUiModel){

        Glide.with(this).load(articleUiModel.urlToImage).apply(
                RequestOptions.placeholderOf(R.drawable.slicinglistingplaceholder)
        ).into(imvArticlePhoto)

        tvArticleTitle.text = articleUiModel.title

        val owner : String = "By : " +articleUiModel.author
        tvArticleOwner.text = owner
        val date : String = AppUtils.formatDate(articleUiModel.publishedAt ,
                AppUtils.DATE_FORMAT_1 , AppUtils.DATE_FORMAT_2)
        tvArticleDate.text = date

        tvArticleDetails.text = articleUiModel.description

        btnOpenWebsite.setOnClickListener {
            AppNavigation.openWebPage(this , articleUiModel.url)
        }
    }

    private fun initToolbar() {
        setSupportActionBar(backToolbar)

        //enable back button
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
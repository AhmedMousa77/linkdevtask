package com.ahmedmousa.linkdevtask.articles.articlesList.domain.di

import com.ahmedmousa.linkdevtask.articles.articlesList.domain.interactor.ArticlesListUseCase
import com.ahmedmousa.linkdevtask.articles.articlesList.domain.repo.ArticlesListRepository
import com.ahmedmousa.linkdevtask.articles.common.presentation.di.scope.ArticlesScope
import dagger.Module
import dagger.Provides

/**
 * class for the domain layer module to provide di for this layer
 * */
@Module
class ArticlesListDomainModule {

    @Provides
    @ArticlesScope
    fun provideArticlesListUseCase(articlesListRepository: ArticlesListRepository)  =  ArticlesListUseCase(articlesListRepository)

}
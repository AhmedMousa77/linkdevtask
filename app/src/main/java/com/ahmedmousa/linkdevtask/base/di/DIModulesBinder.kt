package com.ahmedmousa.linkdevtask.base.di

import com.ahmedmousa.linkdevtask.articles.articlesList.data.di.ArticlesListDataModule
import com.ahmedmousa.linkdevtask.articles.articlesList.domain.di.ArticlesListDomainModule
import com.ahmedmousa.linkdevtask.articles.articlesList.presentation.di.ArticlesListPresentationModule
import com.ahmedmousa.linkdevtask.articles.articlesList.presentation.view.activity.ArticlesActivity
import com.ahmedmousa.linkdevtask.articles.common.presentation.di.scope.ArticlesScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * class for the all modules in the app to provide di to specific modules
 * */
@Module
abstract class DIModulesBinder {

    @ArticlesScope
    @ContributesAndroidInjector(modules = [ArticlesListPresentationModule::class , ArticlesListDomainModule::class , ArticlesListDataModule::class])
    abstract fun bindArticlesListActivity() : ArticlesActivity

}
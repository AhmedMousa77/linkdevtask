package com.ahmedmousa.linkdevtask.base.data.model

/**
 * class for handling request statue to handle ui according to that statue
 * */
sealed class BaseResponseHandler<T> {

    data class Progress<T>(var loading: Boolean) : BaseResponseHandler<T>()
    data class Success<T>(var data: T) : BaseResponseHandler<T>()
    data class Failure<T>(val e: Throwable) : BaseResponseHandler<T>()

    companion object {
        fun <T> loading(isLoading: Boolean): BaseResponseHandler<T> = Progress(isLoading)

        fun <T> success(data: T): BaseResponseHandler<T> = Success(data)

        fun <T> failure(e: Throwable): BaseResponseHandler<T> = Failure(e)
    }
}
package com.ahmedmousa.linkdevtask.base.data.extensions

import android.arch.lifecycle.MutableLiveData
import com.ahmedmousa.linkdevtask.base.data.model.BaseResponseHandler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject


/**
 * Extension function to convert a Publish subject into a LiveData by subscribing to it.
 **/
fun <T> PublishSubject<T>.toLiveData(compositeDisposable: CompositeDisposable): MutableLiveData<T> {
    val data = MutableLiveData<T>()
    compositeDisposable.add(this.subscribe { t: T -> data.value = t })
    return data
}

/**
 * Extension function to push a failed event with an exception to the observing BaseResponseHandler
 * */
fun <T> PublishSubject<BaseResponseHandler<T>>.failed(e: Throwable) {
    with(this){
        loading(false)
        onNext(BaseResponseHandler.failure(e))
    }
}

/**
 * Extension function to push  a success event with data to the observing BaseResponseHandler
 * */
fun <T> PublishSubject<BaseResponseHandler<T>>.success(t: T) {
    with(this){
        loading(false)
        onNext(BaseResponseHandler.success(t))
    }
}

/**
 * Extension function to push the loading status to the observing BaseResponseHandler
 * */
fun <T> PublishSubject<BaseResponseHandler<T>>.loading(isLoading: Boolean) {
    this.onNext(BaseResponseHandler.loading(isLoading))
}
package com.ahmedmousa.linkdevtask.base.di

import android.app.Application
import android.content.Context
import com.ahmedmousa.linkdevtask.base.data.networking.ApiInterface
import com.ahmedmousa.linkdevtask.base.data.networking.MyScheduler
import com.ahmedmousa.linkdevtask.base.data.networking.MySchedulerImpl
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * class for the app module to provide di for all app
 * */
@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: Application): Context = application.applicationContext

    @Provides
    @Singleton
    fun scheduler(): MyScheduler = MySchedulerImpl()

    @Provides
    @Singleton
    fun getRetrofit() : Retrofit = ApiInterface.create()

}
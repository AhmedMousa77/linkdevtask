package com.ahmedmousa.linkdevtask.base.presentation

import com.ahmedmousa.linkdevtask.base.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

/**
 * this is application class that handle building di process
 * */
class AppApplication : DaggerApplication(){

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().application(this).build()
    }
}
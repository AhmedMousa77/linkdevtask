package com.ahmedmousa.linkdevtask.base.data.networking

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * class for handle retrofit building object
 * */
interface ApiInterface {

    companion object Factory {

        private const val BASE_URL = "https://newsapi.org/v1/"

        const val SOURCE_API_1 = "the-next-web"

        const val SOURCE_API_2 = "associated-press"

        const val API_KEY = "533af958594143758318137469b41ba9"

        private val interceptor : HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
            this.level = HttpLoggingInterceptor.Level.BODY
        }

        private val client : OkHttpClient = OkHttpClient.Builder().apply {
            this.addInterceptor(interceptor)
        }.build()

        fun create(): Retrofit {

            return Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(client)
                    .build()
        }


    }

}
package com.ahmedmousa.linkdevtask.utils

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import com.ahmedmousa.linkdevtask.R

/**
 * class for handles all intent explicit or implicit
 * */
class AppNavigation {

    companion object {

        fun startActivity(context: Context, clazz: Class<*>) {
            val intent = Intent(context, clazz)
            context.startActivity(intent)
        }

        fun startActivity(context: Context, clazz: Class<*>, bundle: Bundle) {
            val intent = Intent(context, clazz)
            intent.putExtras(bundle)
            context.startActivity(intent)
        }

        fun openWebPage(context: Context, url: String) {
            try {
                val webpage = Uri.parse(url)
                val myIntent = Intent(Intent.ACTION_VIEW, webpage)
                context.startActivity(myIntent)
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(context, context.getString(R.string.no_activity_handle_open_url_error), Toast.LENGTH_LONG).show()
                e.printStackTrace()
            }

        }

    }

}
package com.ahmedmousa.linkdevtask.utils

/**
* class for static keys sending in intent
* */
class AppIntentConst{

    companion object {

        const val INTENT_ARTICLE_KEY : String = "article"

    }

}
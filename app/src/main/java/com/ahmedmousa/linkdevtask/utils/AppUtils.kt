package com.ahmedmousa.linkdevtask.utils

import java.text.SimpleDateFormat
import java.util.*

/**
 * class for handles some cases in display related to ui
 * */
class AppUtils {

    companion object {

        const val DATE_FORMAT_1 = "yyyy-MM-dd"
        const val DATE_FORMAT_2 = "MMMM dd, yyyy"

        fun formatDate(dateStr: String, format1: String, format2: String) : String{
            val originalFormat = SimpleDateFormat(format1, Locale.ENGLISH)
            val targetFormat = SimpleDateFormat(format2 , Locale.ENGLISH)
            val date = originalFormat.parse(dateStr)
            return targetFormat.format(date)
        }

    }

}